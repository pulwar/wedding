<?php
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class DesignImg extends \Eloquent implements StaplerableInterface{

	use EloquentTrait;

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['img','order','name','description','show','design_id'];

	public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('img', [
            'styles' => [                      
            'thumb' => '280x240'            
            ]
        ]);

        parent::__construct($attributes);
    }

    public function design()
	{
		return $this->belongsTo('Design');
	}

}