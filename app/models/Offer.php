<?php
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Offer extends \Eloquent implements StaplerableInterface{

	use EloquentTrait;

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['img','order','name','description','show'];

	public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('img', [
            'styles' => [                      
            'thumb' => '170x176'            
            ]
        ]);

        parent::__construct($attributes);
    }

}