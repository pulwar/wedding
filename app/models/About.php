<?php
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class About extends \Eloquent implements StaplerableInterface{

	use EloquentTrait;

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];	

	// Don't forget to fill this array
	protected $fillable = ['head_one','head_two','head_tree','str_one','str_two','str_tree','img','description','contacts'];

	public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('img', [
            'styles' => [                      
            'thumb' => '370x247'            
            ]
        ]);

        parent::__construct($attributes);
    }
}