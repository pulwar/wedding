<?php
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Slider extends \Eloquent implements StaplerableInterface{

	use EloquentTrait;

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['img','order'];

	public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('img', [
            'styles' => [ 
            'medium' => '1600x630',          
            'thumb' => '264x205'            
            ]
        ]);

        parent::__construct($attributes);
    }

}