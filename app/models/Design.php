<?php

class Design extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['name','show','description','order'];

	public function images()
	{
		return $this->hasMany('DesignImg');
	}

}