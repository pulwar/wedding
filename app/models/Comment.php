<?php
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Comment extends \Eloquent implements StaplerableInterface{

	use EloquentTrait;

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['img','name','social','text','show'];

	public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('img', [
            'styles' => [                      
            'thumb' => '93x93'            
            ]
        ]);

        parent::__construct($attributes);
    }

}