<?php

class PolyCategory extends \Eloquent {

	protected $table = 'poly_categories';
	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['name','show','description','order'];

	public function images()
	{
		return $this->hasMany('Polygraf');
	}

}