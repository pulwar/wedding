<?php
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Polygraf extends \Eloquent implements StaplerableInterface{

	use EloquentTrait;

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['img','order','name','description','show','poly_category_id'];

	public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('img', [
            'styles' => [                      
            'thumb' => '280x240'            
            ]
        ]);

        parent::__construct($attributes);
    }

    public function category()
	{
		return $this->belongsTo('PolyCategory');
	}

}