<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAboutsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('abouts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->longText('description');
			$table->string('head_one');
			$table->string('head_two');
			$table->string('head_tree');
			$table->string('str_one');
			$table->string('str_two');
			$table->string('str_tree');
			$table->string('img_file_name')->nullable();
			$table->integer('img_file_size')->nullable();
			$table->string('img_content_type')->nullable();
			$table->timestamp('img_updated_at')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('abouts');
		$table->dropColumn('img_file_name');
		$table->dropColumn('img_file_size');
		$table->dropColumn('img_content_type');
		$table->dropColumn('img_updated_at');
	}

}
