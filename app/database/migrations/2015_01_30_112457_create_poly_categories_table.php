<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePolyCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('poly_categories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer('order')->nullable();
			$table->string('description');
			$table->boolean('show');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('poly_categories');
	}

}
