<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDesignImgsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('design_imgs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer('order')->nullable();
			$table->string('description');
			$table->boolean('show');
			$table->string('img_file_name')->nullable();
			$table->integer('img_file_size')->nullable();
			$table->string('img_content_type')->nullable();
			$table->timestamp('img_updated_at')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('design_imgs');
		$table->dropColumn('img_file_name');
		$table->dropColumn('img_file_size');
		$table->dropColumn('img_content_type');
		$table->dropColumn('img_updated_at');
	}

}
