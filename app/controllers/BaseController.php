<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function index()
	{
		$sliders = Slider::all();

		$page = Page::where('name','=','main')->first();

		$comments = Comment::where('show','=',1)->orderBy(DB::raw('RAND()'))->take(3)->get();
		
		return View::make('frontend.index',compact('sliders','comments','page'));	
	}

	protected function contacts()
	{
		$about = About::find(1);

		$page = Page::where('name','=','contacts')->first();

		return View::make('frontend.contacts',compact('about','page'));	
	}

	protected function design()
	{
		$designs = Design::with('images')->where('show','=',1)->orderBy('order','asc')->paginate(1);

		$page = Page::where('name','=','design')->first();

		return View::make('frontend.design', compact('designs','page'));	
	}

	protected function poly()
	{
		$categories = PolyCategory::with('images')->where('show','=',1)->orderBy('order','asc')->paginate(1);

		$page = Page::where('name','=','polygraphy')->first();

		return View::make('frontend.poly', compact('categories','page'));	
	}

	protected function offers()
	{
		$offers = Offer::where('show','=',1)->orderBy('order','asc')->paginate(4);

		$page = Page::where('name','=','offers')->first();

		return View::make('frontend.offers',compact('offers','page'));	
	}

	protected function about()
	{
		//$offers = Offer::where('show','=',1)->orderBy('order','asc')->paginate(4);
		$about = About::find(1);

		$teams = Team::orderBy('order','asc')->get();

		$page = Page::where('name','=','about')->first();

		return View::make('frontend.about',compact('about','teams','page'));	
	}

	public function addComment()
	{
		$data = Input::all();

		$validator = Validator::make($data = Input::all(), Comment::$rules);

		if ($validator->fails())
		{
			$result['comlited'] = 0;
		} else {
			Comment::create($data);
			$result['comlited'] = 1;
		}

		header('Content-Type: application/json');
		echo json_encode($result);	
		
	}
}
