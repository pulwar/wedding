<?php

class AdminAboutsController extends \BaseController {	

	/**
	 * Show the form for editing the specified about.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit()
	{
		$id = 1;

		$about = About::find($id);

		return View::make('admin.abouts.edit', compact('about'));
	}

	/**
	 * Update the specified about in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{	
		
		$about = About::findOrFail($id);

		$validator = Validator::make($data = Input::all(), About::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$about->update($data);

		return Redirect::route('admin.abouts.edit');
	}	

}
