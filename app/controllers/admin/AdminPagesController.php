<?php

class AdminPagesController extends \BaseController {

	/**
	 * Display a listing of pages
	 *
	 * @return Response
	 */
	public function index()
	{
		$page = Page::all();

		$sidebar_items = array(
            "Каталог" => array('url' => URL::route('admin.pages.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
        );

		return View::make('admin.pages.index', compact('pages','sidebar_items'));
	}
	
	/**
	 * Show the form for editing the specified page.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$page = Page::find($id);

		$sidebar_items = array(
            "Каталог" => array('url' => URL::route('admin.pages.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
        );

		return View::make('admin.pages.edit', compact('page','sidebar_items'));
	}

	/**
	 * Update the specified page in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$page = Page::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Page::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$page->update($data);

		return Redirect::route('admin.pages.index');
	}

	

}
