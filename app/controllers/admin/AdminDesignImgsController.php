<?php

class AdminDesignImgsController extends \BaseController {

	/**
	 * Display a listing of designimgs
	 *
	 * @return Response
	 */
	public function index()
	{
		$designimgs = Designimg::all();
		return View::make('admin.designimgs.index', compact('designimgs'));
	}

	/**
	 * Show the form for creating a new designimg
	 *
	 * @return Response
	 */
	public function create()	
	{
		$sidebar_items = array(
            "Коллекции" => array('url' => URL::route('admin.designs.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новая коллекция' => array('url' => URL::route('admin.designs.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
            'Новое фото' => array('url' => URL::route('admin.designimgs.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
        );

		return View::make('admin.designimgs.create', compact('sidebar_items'));
	}

	/**
	 * Store a newly created designimg in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Designimg::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$designimg = Designimg::create($data);

		$sidebar_items = array(
            "Коллекции" => array('url' => URL::route('admin.designs.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новая коллекция' => array('url' => URL::route('admin.designs.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
            'Новое фото' => array('url' => URL::route('admin.designimgs.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
        );
		
		return Redirect::route('admin.designimgs.show',array($designimg->design_id));
	}

	/**
	 * Display the specified designimg.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$designimgs = Designimg::with('design')->where('design_id', '=',$id)->get();
			
		$design = Design::find($id);

		$sidebar_items = array(
            "Коллекции" => array('url' => URL::route('admin.designs.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новая коллекция' => array('url' => URL::route('admin.designs.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
            'Новое фото' => array('url' => URL::route('admin.designimgs.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
        );

		return View::make('admin.designimgs.index', compact('designimgs','design','sidebar_items'));
	}

	/**
	 * Show the form for editing the specified designimg.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$designimg = Designimg::find($id);

		$sidebar_items = array(
            "Коллекции" => array('url' => URL::route('admin.designs.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новая коллекция' => array('url' => URL::route('admin.designs.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
            'Новое фото' => array('url' => URL::route('admin.designimgs.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
        );

		return View::make('admin.designimgs.edit', compact('designimg','sidebar_items'));
	}

	/**
	 * Update the specified designimg in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$designimg = Designimg::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Designimg::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$designimg->update($data);		

		return Redirect::route('admin.designimgs.show',array($designimg->design_id));
	}

	/**
	 * Remove the specified designimg from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Designimg::destroy($id);

		return Redirect::back();
	}

}
