<?php

class AdminSlidersController extends \BaseController {

	/**
	 * Display a listing of sliders
	 *
	 * @return Response
	 */
	public function index()
	{
		$sliders = Slider::all();

		$sidebar_items = array(
            "Каталог" => array('url' => URL::route('admin.sliders.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новый слайдер' => array('url' => URL::route('admin.sliders.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),            
        );

		return View::make('admin.sliders.index', compact('sliders','sidebar_items'));
	}

	/**
	 * Show the form for creating a new slider
	 *
	 * @return Response
	 */
	public function create()
	{
		$sidebar_items = array(
            "Каталог" => array('url' => URL::route('admin.sliders.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новый слайдер' => array('url' => URL::route('admin.sliders.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),            
        );

		return View::make('admin.sliders.create',compact('sidebar_items'));
	}

	/**
	 * Store a newly created slider in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Slider::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Slider::create($data);

		return Redirect::route('admin.sliders.index');
	}

	/**
	 * Display the specified slider.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$slider = Slider::findOrFail($id);

		$sidebar_items = array(
            "Каталог" => array('url' => URL::route('admin.sliders.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новый слайдер' => array('url' => URL::route('admin.sliders.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),            
        );

		return View::make('admin.sliders.show', compact('slider','sidebar_items'));
	}

	/**
	 * Show the form for editing the specified slider.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$slider = Slider::find($id);

		$sidebar_items = array(
            "Каталог" => array('url' => URL::route('admin.sliders.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новый слайдер' => array('url' => URL::route('admin.sliders.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),            
        );

		return View::make('admin.sliders.edit', compact('slider','sidebar_items'));
	}

	/**
	 * Update the specified slider in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$slider = Slider::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Slider::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$slider->update($data);

		return Redirect::route('admin.sliders.index');
	}

	/**
	 * Remove the specified slider from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Slider::destroy($id);

		return Redirect::route('admin.sliders.index');
	}

}
