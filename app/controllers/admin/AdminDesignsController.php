<?php

class AdminDesignsController extends \BaseController {

	/**
	 * Display a listing of designs
	 *
	 * @return Response
	 */
	public function index()
	{
		$designs = Design::orderBy('order','asc')->get();

		$sidebar_items = array(
            "Коллекции" => array('url' => URL::route('admin.designs.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новая коллекция' => array('url' => URL::route('admin.designs.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
            'Новое фото' => array('url' => URL::route('admin.designimgs.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
        );

		return View::make('admin.designs.index', compact('designs','sidebar_items'));
	}

	/**
	 * Show the form for creating a new design
	 *
	 * @return Response
	 */
	public function create()
	{
		$sidebar_items = array(
            "Коллекции" => array('url' => URL::route('admin.designs.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новая коллекция' => array('url' => URL::route('admin.designs.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
            'Новое фото' => array('url' => URL::route('admin.designimgs.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
        );

		return View::make('admin.designs.create', compact('sidebar_items'));
	}

	/**
	 * Store a newly created design in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Design::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Design::create($data);

		return Redirect::route('admin.designs.index');
	}

	/**
	 * Display the specified design.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$design = Design::findOrFail($id);

		$sidebar_items = array(
            "Коллекции" => array('url' => URL::route('admin.designs.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новая коллекция' => array('url' => URL::route('admin.designs.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
            'Новое фото' => array('url' => URL::route('admin.designimgs.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
        );

		return View::make('admin.designs.show', compact('design','sidebar_items'));
	}

	/**
	 * Show the form for editing the specified design.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$design = Design::find($id);

		$sidebar_items = array(
            "Коллекции" => array('url' => URL::route('admin.designs.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новая коллекция' => array('url' => URL::route('admin.designs.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
            'Новое фото' => array('url' => URL::route('admin.designimgs.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
        );

		return View::make('admin.designs.edit', compact('design','sidebar_items'));
	}

	/**
	 * Update the specified design in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$design = Design::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Design::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$design->update($data);

		return Redirect::route('admin.designs.index');
	}

	/**
	 * Remove the specified design from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Design::destroy($id);

		return Redirect::route('admin.designs.index');
	}

	public function changeOrderOfDesign()
	{
		if (isset($_POST['arr'])){
			$i = 1;
			foreach ($_POST['arr'] as $value) {
				$design = Design::find($value);
				$design->order = $i;
				$design->save();
				$i++;
			}
			$result = 1;
		} else {
			$result = 0;
		}
		header('Content-Type: application/json');
		echo json_encode($result);
	}

}
