<?php

class AdminOffersController extends \BaseController {

	/**
	 * Display a listing of offers
	 *
	 * @return Response
	 */
	public function index()
	{
		$offers = Offer::orderBy('order','asc')->get();

		$sidebar_items = array(
            "Предложения" => array('url' => URL::route('admin.offers.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новое' => array('url' => URL::route('admin.offers.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),            
        );

		return View::make('admin.offers.index', compact('offers','sidebar_items'));
	}

	/**
	 * Show the form for creating a new offer
	 *
	 * @return Response
	 */
	public function create()
	{	
		$sidebar_items = array(
            "Предложения" => array('url' => URL::route('admin.offers.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новое' => array('url' => URL::route('admin.offers.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),            
        );

		return View::make('admin.offers.create',compact('sidebar_items'));
	}

	/**
	 * Store a newly created offer in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Offer::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Offer::create($data);

		return Redirect::route('admin.offers.index');
	}

	/**
	 * Display the specified offer.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$offer = Offer::findOrFail($id);

		$sidebar_items = array(
            "Предложения" => array('url' => URL::route('admin.offers.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новое' => array('url' => URL::route('admin.offers.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),            
        );

		return View::make('admin.offers.show', compact('offer','sidebar_items'));
	}

	/**
	 * Show the form for editing the specified offer.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$offer = Offer::find($id);

		$sidebar_items = array(
            "Предложения" => array('url' => URL::route('admin.offers.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новое' => array('url' => URL::route('admin.offers.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),            
        );

		return View::make('admin.offers.edit', compact('offer','sidebar_items'));
	}

	/**
	 * Update the specified offer in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$offer = Offer::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Offer::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$offer->update($data);

		return Redirect::route('admin.offers.index');
	}

	/**
	 * Remove the specified offer from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Offer::destroy($id);

		return Redirect::route('admin.offers.index');
	}

	public function changeOrder()
	{
		if (isset($_POST['arr'])){
			$i = 1;
			foreach ($_POST['arr'] as $value) {
				$offer = Offer::find($value);
				$offer->order = $i;
				$offer->save();
				$i++;
			}
			$result = 1;
		} else {
			$result = 0;
		}
		header('Content-Type: application/json');
		echo json_encode($result);
	}

}
