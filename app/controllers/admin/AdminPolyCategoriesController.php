<?php

class AdminPolyCategoriesController extends \BaseController {

	/**
	 * Display a listing of polycategories
	 *
	 * @return Response
	 */
	public function index()
	{
		$polycategories = Polycategory::orderBy('order','asc')->get();

		$sidebar_items = array(
            "Категории" => array('url' => URL::route('admin.polycategories.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новая категория' => array('url' => URL::route('admin.polycategories.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
            'Новое фото' => array('url' => URL::route('admin.polygrafs.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
        );


		return View::make('admin.polycategories.index', compact('polycategories','sidebar_items'));
	}

	/**
	 * Show the form for creating a new polycategory
	 *
	 * @return Response
	 */
	public function create()
	{
		$sidebar_items = array(
            "Категории" => array('url' => URL::route('admin.polycategories.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новая категория' => array('url' => URL::route('admin.polycategories.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
            'Новое фото' => array('url' => URL::route('admin.polygrafs.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
        );

		return View::make('admin.polycategories.create',compact('sidebar_items'));
	}

	/**
	 * Store a newly created polycategory in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Polycategory::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Polycategory::create($data);

		return Redirect::route('admin.polycategories.index');
	}
	

	/**
	 * Show the form for editing the specified polycategory.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$polycategory = Polycategory::find($id);

		$sidebar_items = array(
            "Категории" => array('url' => URL::route('admin.polycategories.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новая категория' => array('url' => URL::route('admin.polycategories.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
            'Новое фото' => array('url' => URL::route('admin.polygrafs.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
        );

		return View::make('admin.polycategories.edit', compact('polycategory','sidebar_items'));
	}

	/**
	 * Update the specified polycategory in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$polycategory = Polycategory::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Polycategory::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$polycategory->update($data);

		return Redirect::route('admin.polycategories.index');
	}

	/**
	 * Remove the specified polycategory from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Polycategory::destroy($id);

		return Redirect::route('admin.polycategories.index');
	}

	public function changeOrderOfDesign()
	{
		if (isset($_POST['arr'])){
			$i = 1;
			foreach ($_POST['arr'] as $value) {
				$category = Polycategory::find($value);
				$category->order = $i;
				$category->save();
				$i++;
			}
			$result = 1;
		} else {
			$result = 0;
		}
		header('Content-Type: application/json');
		echo json_encode($result);
	}

}
