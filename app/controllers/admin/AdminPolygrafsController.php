<?php

class AdminPolygrafsController extends \BaseController {

	
	/**
	 * Show the form for creating a new polygraf
	 *
	 * @return Response
	 */
	public function create()
	{
		$sidebar_items = array(
            "Категории" => array('url' => URL::route('admin.polycategories.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новая категория' => array('url' => URL::route('admin.polycategories.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
            'Новое фото' => array('url' => URL::route('admin.polygrafs.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
        );

		return View::make('admin.polygrafs.create', compact('sidebar_items'));
	}

	/**
	 * Store a newly created polygraf in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Polygraf::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$polygraf = Polygraf::create($data);

		return Redirect::route('admin.polygrafs.show',array($polygraf->poly_category_id));
	}

	/**
	 * Display the specified polygraf.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$polygrafs = Polygraf::with('category')->where('poly_category_id', '=',$id)->get();

		$category = PolyCategory::findOrFail($id);

		$sidebar_items = array(
            "Категории" => array('url' => URL::route('admin.polycategories.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новая категория' => array('url' => URL::route('admin.polycategories.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
            'Новое фото' => array('url' => URL::route('admin.polygrafs.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
        );

		return View::make('admin.polygrafs.index', compact('polygrafs','category','sidebar_items'));
	}

	/**
	 * Show the form for editing the specified polygraf.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$polygraf = Polygraf::find($id);

		$sidebar_items = array(
            "Категории" => array('url' => URL::route('admin.polycategories.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новая категория' => array('url' => URL::route('admin.polycategories.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
            'Новое фото' => array('url' => URL::route('admin.polygrafs.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
        );

		return View::make('admin.polygrafs.edit', compact('polygraf','sidebar_items'));
	}

	/**
	 * Update the specified polygraf in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$polygraf = Polygraf::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Polygraf::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$polygraf->update($data);

		return Redirect::route('admin.polygrafs.show',array($polygraf->poly_category_id));
	}

	/**
	 * Remove the specified polygraf from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Polygraf::destroy($id);

		return Redirect::back();
	}	

}
