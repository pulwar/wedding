<?php

class AdminTeamsController extends \BaseController {

	/**
	 * Display a listing of teams
	 *
	 * @return Response
	 */
	public function index()
	{
		$teams = Team::orderBy('order','asc')->get();

		$sidebar_items = array(
            "Каталог" => array('url' => URL::route('admin.teams.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новое фото' => array('url' => URL::route('admin.teams.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),            
        );

		return View::make('admin.teams.index', compact('teams','sidebar_items'));
	}

	/**
	 * Show the form for creating a new team
	 *
	 * @return Response
	 */
	public function create()
	{
		$sidebar_items = array(
            "Каталог" => array('url' => URL::route('admin.teams.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новое фото' => array('url' => URL::route('admin.teams.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),            
        );

		return View::make('admin.teams.create',compact('sidebar_items'));
	}

	/**
	 * Store a newly created team in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Team::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Team::create($data);

		return Redirect::route('admin.teams.index');
	}
	

	/**
	 * Show the form for editing the specified team.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$team = Team::find($id);

		$sidebar_items = array(
            "Каталог" => array('url' => URL::route('admin.teams.index'), 'icon' => '<i class="fa fa-shopping-cart"></i>'),
            'Новое фото' => array('url' => URL::route('admin.teams.create'), 'icon' => '<i class="fa fa-plus-circle"></i>'),            
        );

		return View::make('admin.teams.edit', compact('team','sidebar_items'));
	}

	/**
	 * Update the specified team in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$team = Team::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Team::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$team->update($data);

		return Redirect::route('admin.teams.index');
	}

	/**
	 * Remove the specified team from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Team::destroy($id);

		return Redirect::route('admin.teams.index');
	}

	public function orderUpdate()
	{
		if (isset($_POST['arr'])){
			$i = 1;
			foreach ($_POST['arr'] as $value) {
				$team = Team::find($value);
				$team->order = $i;
				$team->save();
				$i++;
			}
			$result = 1;
		} else {
			$result = 0;
		}
		header('Content-Type: application/json');
		echo json_encode($result);
	}

}
