@extends('frontend._layouts.default')
@section('title'){{$page->head}}@stop
@section('keywords'){{$page->mKeywords}}@stop
@section('description'){{$page->mDescription}}@stop
@section('content')
@section('content')
<!--content-->
<div class="global indent" style="border-top: 1px solid #d2d2d2;">
  <div class="container">
      <h4 class="center">Специальные предложения</h4>
      <div class="row">   
          <div class="thumb-box7">
              <div class="col-lg-6 col-md-12 col-sm-12">
                  {{--*/ $i=1 /*--}}
                  @foreach($offers as $offer)
                  @if($i % 2 == 1)
                    <div class="thumb-pad3 maxheight color{{rand (1, 9)}}">
                      <div class="thumbnail">
                          <figure><img src="{{$offer->img->url('thumb') }}" alt=""></figure>
                          <div class="caption">
                              <p>{{$offer->name}}</p>
                          </div>
                      </div>                      
                      {{$offer->description}}
                    </div>
                    @endif
                  {{--*/ $i++ /*--}}
                  @endforeach                  
              </div>
              <div class="col-lg-6 col-md-12 col-sm-12">
                  {{--*/ $i=1 /*--}}
                  @foreach($offers as $offer)
                  @if($i % 2 != 1)
                    <div class="thumb-pad3 maxheight color{{rand (1, 9)}}">
                        <div class="thumbnail">
                            <figure><img src="{{$offer->img->url('thumb') }}" alt=""></figure>
                            <div class="caption">
                                <p>{{$offer->name}}</p>
                            </div>
                        </div>                      
                        {{$offer->description}}
                    </div> 
                  @endif
                  {{--*/ $i++ /*--}} 
                  @endforeach                  
              </div>
          </div>
          <div>
              <div class="paginator" style="text-align: center;">
                {{$offers->appends(Input::except(['page']) )->links()}}  
              </div>
            </div>
      </div>
  </div>  
</div>
@stop
@section('add-css')
  
@stop
@section('add-js')
  
@stop