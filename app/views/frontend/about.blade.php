@extends('frontend._layouts.default')
@section('title'){{$page->head}}@stop
@section('keywords'){{$page->mKeywords}}@stop
@section('description'){{$page->mDescription}}@stop
@section('content')
@section('content')
<div class="global indent">
    <!--content-->
    <div class="container">
        <div class="who-box">
            <div class="col-lg-4 col-md-4 col-sm-6">
                <h4>Кто мы</h4>
                <div class="thumb-pad4">
                    <div class="thumbnail">
                        <figure><img src="{{ $about->img->url('thumb') }}" alt=""></figure>
                        <div class="caption">
                            {{$about->description}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <h4>Наша цель</h4>
                <ul class="list1">
                    <li>
                        <div class="extra-wrap">
                            <div class="badge">1.</div>
                            <div class="extra-wrap">
                                <p class="title">{{$about->head_one}}</p>
                            </div>
                        </div>
                        <p>{{$about->str_one}}</p>
                    </li>
                    <li>
                        <div class="extra-wrap">
                            <div class="badge">2.</div>
                            <div class="extra-wrap">
                                <p class="title">{{$about->head_two}}</p>
                            </div>
                        </div>
                        <p>{{$about->str_two}}</p>
                    </li>
                    <li>
                        <div class="extra-wrap">
                            <div class="badge">3.</div>
                            <div class="extra-wrap">
                                <p class="title">{{$about->head_tree}}</p>
                            </div>
                        </div>
                        <p>{{$about->str_tree}}</p>
                    </li>
                </ul>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <h4>Команда</h4>
                <div class="row">
                    @foreach($teams as $team)
                    <div class="col-lg-6 col-md-6 col-sm-4 col-xs-4 trainerBox">
                        <figure><a href="{{ $team->img->url() }}"><img src="{{ $team->img->url('thumb') }}" alt=""></a></figure>
                    </div>
                    @endforeach                    
                </div>
            </div>
        </div>
    </div>    
</div>
@stop
@section('add-css')
  {{HTML::style('css/design/touchTouch.css')}}
@stop
@section('add-js')
{{HTML::script('js/script.js')}} 
{{HTML::script('js/touchTouch.jquery.js')}}
<script>
   $(window).load(function() {
     // Initialize the gallery
    $('.trainerBox figure a').touchTouch();
  });
</script>  
@stop