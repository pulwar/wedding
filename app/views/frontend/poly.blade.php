@extends('frontend._layouts.default')
@section('title'){{$page->head}}@stop
@section('keywords'){{$page->mKeywords}}@stop
@section('description'){{$page->mDescription}}@stop
@section('content')
@section('content')
<section class="content">
    <div class="container_16">
        <div class="row">
            @foreach($categories as $poly)
              <div class="grid_16">
                  <h2>{{$poly->name}}</h2>
                  <div class="text-1">{{$poly->description}}</div>
              </div>
              <div class="gallery clearfix">
                  @foreach($poly->images as $image)
                    <div class="grid_5 box-2 maxheight">
                        <a href="{{ $image->img->url() }}" class="magnifier"><img src="{{ $image->img->url('thumb') }}" alt="" class="wrapper"></a>
                        <p class="title-1">{{$image->name}}</p>
                        <div>{{$image->description}}</div>
                    </div>
                  @endforeach
              </div>
            @endforeach
            <div>
              <div class="paginator" style="text-align: center;">
                {{$categories->appends(Input::except(['page']) )->links()}}  
              </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('add-css')
  {{HTML::style('css/design/style.css')}}
  <!--{{HTML::style('css/design/superfish.css')}}-->
  {{HTML::style('css/design/touchTouch.css')}}

@stop
@section('add-js') 
  {{HTML::script('js/script.js')}}  
  {{HTML::script('js/touchTouch.jquery.js')}}
  <script>
        $(window).load(function(){
            $(".magnifier").touchTouch();
        })
    </script>
@stop