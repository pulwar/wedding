@extends('frontend._layouts.default')
@section('title'){{$page->head}}@stop
@section('keywords'){{$page->mKeywords}}@stop
@section('description'){{$page->mDescription}}@stop
@section('content')
<div class="global">
<div class="slider">
    <em></em>
    <strong></strong>
    <div class="camera_wrap">
        @foreach($sliders as $slider)
            <div data-src="{{ $slider->img->url('medium') }}"></div>
        @endforeach
    </div>
</div>
<!--content-->
<div class="container">    
</div> 
<div class="thumb-box3">
    <em></em>
    <strong></strong>
    <div class="container">
        <p class="title wow fadeIn">Exquisite wedding cakes <br>from cake artists</p>
        <h3 class="wow fadeIn">Sociistoque pentibus et magnis diarturto ascetur rimulug aleada</h3>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 wow fadeInLeft" data-wow-delay="0.4s">
                <div class="thumb-pad1">
                    <div class="thumbnail">
                        <figure><img src="img/page1_icon1.png" alt=""></figure>
                        <div class="caption">
                            <p class="title">Специальные предложения</p>
                        </div>
                    </div>
                    <h3>Уникальные сборки</h3>
                    <p>Уже проверенные варианты. Высочайший уровень исполнения!</p>
                    <a href="{{URL::route('offers')}}" class="btn-default btn2">посмотреть</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 wow fadeInLeft" data-wow-delay="0.2s">
                <div class="thumb-pad1">
                    <div class="thumbnail">
                        <figure><img src="img/page1_icon2.png" alt=""></figure>
                        <div class="caption">
                            <p class="title color1">Полиграфия</p>
                        </div>
                    </div>
                    <h3>Дизайн и печать</h3>
                    <p>Наша продукция помогает сделать этот день особенным не только для вас, но и для ваших близких!</p>
                    <a href="{{URL::route('polygraphy')}}" class="btn-default btn3">посмотреть</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 wow fadeInLeft">
                <div class="thumb-pad1">
                    <div class="thumbnail">
                        <figure><img src="img/page1_icon3.png" alt=""></figure>
                        <div class="caption">
                            <p class="title color2">Оформление залов</p>
                        </div>
                    </div>
                    <h3>Самые привлекательные варианты</h3>
                    <p>Мы вкладываем в работу частичку души. Чудеса происходят на ваших глазач.</p>
                    <a href="{{URL::route('design')}}" class="btn-default btn4">посмотреть</a>
                </div>
            </div>
        </div>
    </div>
</div> 
<div class="thumb-box4">
    <em></em>
    <div class="container">
        <p class="title wow fadeIn">Мы гордимся тем, что мы делаем <br>многие клиенты счастливы!</p>
        <div class="row">
            {{--*/$wowDelay=0.4;/*--}}
            @foreach($comments as $comment)
            <div class="col-lg-4 col-md-4 col-sm-4 wow fadeInLeft" data-wow-delay="{{$wowDelay}}s">
                <div class="thumb-pad2">
                    <div class="thumbnail">
                        <figure><img src="{{$comment->img->url('thumb')}}" alt=""></figure>
                        <div class="caption">
                            <p class="name">{{$comment->name}}</p>                            
                        </div>
                    </div>
                    <div class="caption2">
                        <p>{{$comment->text}}</p>
                        <em></em>
                    </div>
                </div>
            </div>
            {{--*/$wowDelay-=0.2;/*--}}
            @endforeach                      
        </div>
    </div>
    
</div>
<!--div class="map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d24214.807650104907!2d-73.94846048422478!3d40.65521573400813!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sus!4v1395650655094" style="border:0"></iframe>
</div-->
</div>
@stop
@section('add-js')
<!--script src="js/TMForm.js"></script-->
<script src="js/wow/wow.js"></script>
<script src="js/wow/device.min.js"></script>
<script>
    $(document).ready(function(){
        jQuery('.camera_wrap').camera();
    });
</script>
<script>
    $(document).ready(function () {       
      if ($('html').hasClass('desktop')) {
        new WOW().init();
      }   
    });
</script>
<!--[if lt IE 9]>
    <div style='text-align:center'><a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a></div>  
  <![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->
@stop