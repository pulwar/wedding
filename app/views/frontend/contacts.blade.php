@extends('frontend._layouts.default')
@section('title'){{$page->head}}@stop
@section('keywords'){{$page->mKeywords}}@stop
@section('description'){{$page->mDescription}}@stop
@section('content')
@section('content')
<!--content-->
<div class="global indent">
    <div class="container">
        <div class="map2">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d24214.807650104907!2d-73.94846048422478!3d40.65521573400813!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sus!4v1395650655094" style="border:0"></iframe>
        </div>
    </div>
    <div class="formBox">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <h4>Контакты</h4>
                    <div class="info">
                        {{$about->contacts}}
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8">
                    <h4>Оставить отзыв</h4>
                    <form id="contact-form">
                          <div class="contact-form-loader"></div>
                          <fieldset>
                            <label class="name form-div-1">
                              <strong>ФИО*</strong>
                              <input type="text" name="name" placeholder="" value="" data-constraints="@Required @JustLetters"  />
                              <span class="empty-message">*Поле необходимо заполнить.</span>
                              <span class="error-message">*This is not a valid name.</span>
                            </label>
                            <label class="phone form-div-3">
                              <strong>Ссылка на профиль в соцсетях</strong>
                              <input type="text" name="social" placeholder="" value="" data-constraints="@JustNumbers" />
                              <span class="empty-message">*This field is required.</span>
                              <span class="error-message">*This is not a valid phone.</span>
                            </label>
                            <label class="message form-div-4">
                              <strong>Сообщение*</strong>
                              <textarea name="text" placeholder="" data-constraints='@Required @Length(min=20,max=999999)'></textarea>
                              <span class="empty-message">*Поле необходимо заполнить.</span>
                              <span class="error-message">*The message is too short.</span>
                            </label>
                            <!-- <label class="recaptcha"><span class="empty-message">*This field is required.</span></label> -->
                            <div>
                              <a href="#" data-type="submit" class="btn-default btn1">Отправить</a>
                              <p class="description">* обязательные поля</p>
                            </div>
                          </fieldset> 
                          <div class="modal fade response-message">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                  <h4 class="modal-title">Modal title</h4>
                                </div>
                                <div class="modal-body">
                                  You message has been sent! We will be in touch soon.
                                </div>      
                              </div>
                            </div>
                          </div>
                    </form>
                </div>
            </div>
        </div>
    
@stop
@section('add-css')
  {{HTML::style('css/sweet-alert.css')}}
@stop
@section('add-js')
  {{HTML::script('js/sweet-alert.js')}}
<script type="text/javascript">
  $(document).ready(function(){    

    $('body').on('click', 'a.btn1', function(){
      if($('input[name="name"]').val()=='') {        
        $('input[name="name"]').focus();
        return false;
      } else if($('textarea[name="text"]').val()=='') {
        $('textarea[name="text"]').focus();
        return false;
      }
      console.log('send');
      $.ajax({        
              url: './add-comment',
              type:'POST',
              data:$('form').serialize(),
              dataType : 'json',
              success: function(data){
                  if(data['comlited']){
                    $('form')[0].reset();
                    swal("Спасибо!", "Ваш отзыв отправлен модератеру!", "success");
                  } 
              }                
          });
      return false;
    })
  });
</script>
@stop