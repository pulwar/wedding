<!DOCTYPE html>
<html lang="en">
<head>
<title>@yield('title')</title>
<meta charset="utf-8">    
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="{{ URL::to('img/favicon.ico') }}" type="image/x-icon">
<link rel="shortcut icon" href="{{ URL::to('img/favicon.ico') }}" type="image/x-icon" />
<meta name="description" content="@yield('description')">
<meta name="keywords" content="@yield('keywords')">
<meta name="author" content="Your name">
<meta name = "format-detection" content = "telephone=no" />
<!--CSS-->
<link rel="stylesheet" href="css/bootstrap.css" >
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/camera.css">
<link rel="stylesheet" href="fonts/font-awesome.css">
<link rel="stylesheet" href="css/contact-form.css">
@yield('add-css')
<!--JS-->
<script src="js/jquery.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/superfish.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/jquery.mobilemenu.js"></script>
<script src="js/jquery.equalheights.js"></script>
<script src="js/camera.js"></script>
</head>
<body>

@include('frontend._layouts.header')  
@yield('content')

<!--footer-->
<footer>
    <em></em>
    <div class="container">
        <p>CACY &copy; <em id="copyright-year"></em> &bull; <a href="index-5.html">Privacy policy</a></p>
    </div>
  <!-- {%FOOTER_LINK} -->
</footer>
<script src="js/bootstrap.min.js"></script>
<script src="js/tm-scripts.js"></script>
@yield('add-js')
</body>
</html>