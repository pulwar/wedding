<!--header-->
<header class="indent">    
    <div class="container">
        <h1 class="navbar-brand navbar-brand_"><a href="index.html"><img src="img/logo.png" alt="logo"></a></h1>
        <nav class="navbar navbar-default navbar-static-top tm_navbar clearfix" role="navigation">
            <ul class="nav sf-menu clearfix">
                <li {{Route::getCurrentRoute()->getName()=='index' ? 'class="active"' : ''}}><a href="{{URL::route('index')}}">Главная</a><em></em></li>
                <li class="sub-menu{{Route::getCurrentRoute()->getName()=='design' ? ' active' : ''}}{{Route::getCurrentRoute()->getName()=='polygraphy' ? ' active' : ''}}"><a href="#">Наша работа</a><span></span><em></em>
                    <ul class="submenu">                        
                        <li><a href="{{URL::route('design')}}">Оформление</a></li>
                        <li><a href="{{URL::route('polygraphy')}}">Полиграфия</a></li>                        
                    </ul>
                </li>
                <li {{Route::getCurrentRoute()->getName()=='about' ? 'class="active"' : ''}}><a href="{{URL::route('about')}}">О нас</a><em></em></li>
                <li {{Route::getCurrentRoute()->getName()=='offers' ? 'class="active"' : ''}}><a href="{{URL::route('offers')}}">Спец предложения</a><em></em></li>
                <li {{Route::getCurrentRoute()->getName()=='contacts' ? 'class="active"' : ''}}><a href="{{URL::route('contacts')}}">Контакты</a></li>
            </ul>
        </nav>
    </div>
</header>