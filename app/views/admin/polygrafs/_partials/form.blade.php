<div class="form-group">
	{{ Form::label('poly_category_id','Категория') }}
	{{ Form::select('poly_category_id',PolyCategory::lists('name','id'),null,['class' => 'form-control'] ) }}
</div>
<span class="text-danger">{{$errors->first('poly_category_id')}}</span>
<div class="form-group">
	{{ Form::label('show','Отображать?') }}
	{{ Form::select('show',array('1' => 'Да','0' => 'Нет'),null,['class' => 'form-control']) }}	
</div>
<span class="text-danger">{{$errors->first('show')}}</span>
<div class="form-group">
	{{ Form::label('name','Подпись') }}
	{{ Form::text('name',null,['class' => 'form-control']) }}	
</div>
<span class="text-danger">{{$errors->first('name')}}</span>
<div class="form-group">
	{{ Form::label('description','Описание') }}
	{{ Form::textarea('description',null,['class' => 'form-control']) }}	
</div>
<span class="text-danger">{{$errors->first('description')}}</span>
<div class="form-group">
	{{ Form::file('img')}}
</div>
{{Form::submit('Сохранить', array("class"=>"btn btn-info pull-right "))}}
