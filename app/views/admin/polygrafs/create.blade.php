@extends('laravel-authentication-acl::admin.layouts.base-2cols')

@section('title')
Admin area: dashboard
@stop

@section('content')	
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title bariol-thin"><i class="fa fa-pencil"> Новая фотография</i></h3>
            </div>            
            <div class="panel-body">
			{{ Form::open(array('route' => 'admin.polygrafs.store','method' => 'post','files' => true))}}
				@include('admin.polygrafs._partials.form')
			{{ Form::close()}}	
			</div>
        </div>       
    </div>
</div>	
@stop 