@extends('laravel-authentication-acl::admin.layouts.base-2cols')

@section('title')
Admin area: dashboard
@stop

@section('content')	
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title bariol-thin"><i class="fa fa-pencil"> Коллекция: {{$design->name}}</i></h3>
            </div>            
            <div class="panel-body">
			<div class="row">
                <div class="col-md-12 margin-bottom-12">
                    <a href="{{URL::route('admin.designimgs.create')}}" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Добавить</a>
                </div>
            </div>
            @if(  count($designimgs) > 0 )
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Название</th>                       
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($designimgs as $designimg)
                        <tr>
                            <td style="width:80%">{{$designimg->name}}</td>                            
                            <td style="witdh:10%">{{$designimg->show ? '<i class="fa fa-eye"></i>' : ''}}</td>
                            <td style="witdh:10%">                               
                                <a href="{{URL::route('admin.designimgs.edit', ['id' => $designimg->id])}}"><i class="fa fa-pencil-square-o fa-2x"></i></a>
                                <a href="{{URL::route('admin.designimgs.destroy',['id' => $designimg->id, '_token' => csrf_token()])}}" class="margin-left-5"><i class="fa fa-trash-o delete fa-2x"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>                
            @else
            <span class="text-warning"><h5>Нет коллекций.</h5></span>
            @endif
			</div>
        </div>       
    </div>
</div>	
@stop 