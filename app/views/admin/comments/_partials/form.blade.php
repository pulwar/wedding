<div class="form-group">
	{{ Form::label('show','Отображать?') }}
	{{ Form::select('show',array('0' => 'Нет','1' => 'Да'),null,['class' => 'form-control']) }}	
</div>
<span class="text-danger">{{$errors->first('show')}}</span>
<div class="form-group">
	{{ Form::label('name','ФИО') }}
	{{ Form::text('name',null,['class' => 'form-control']) }}	
</div>
<span class="text-danger">{{$errors->first('name')}}</span>
<div class="form-group">
	{{ Form::label('text','Комментарий') }}
	{{ Form::textarea('text',null,['class' => 'form-control']) }}	
</div>
<span class="text-danger">{{$errors->first('text')}}</span>
<div class="form-group">
	{{ Form::label('social','Ссылка на соцсети') }}
	{{ Form::text('social',null,['class' => 'form-control']) }}	
</div>
<span class="text-danger">{{$errors->first('social')}}</span>
<div class="form-group">
	{{ Form::file('img')}}
</div>
{{Form::submit('Сохранить', array("class"=>"btn btn-info pull-right "))}}
