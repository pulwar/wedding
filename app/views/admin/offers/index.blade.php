@extends('laravel-authentication-acl::admin.layouts.base-2cols')

@section('title')
Admin area: dashboard
@stop

@section('content')	
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title bariol-thin"><i class="fa fa-pencil"> Спец предложения</i></h3>
            </div>            
            <div class="panel-body">
			<div class="row">
                <div class="col-md-12 margin-bottom-12">
                    <a href="{{URL::route('admin.offers.create')}}" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Добавить</a>
                </div>
            </div>
            @if(  count($offers) > 0 )
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Загаловок</th>                       
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody id="sortable">
                        @foreach($offers as $offer)
                        <tr id="{{$offer->id}}">
                            <td style="width:80%">{{$offer->name}}</td>                            
                            <td style="witdh:10%">{{$offer->show ? '<i class="fa fa-eye"></i>' : ''}}</td>
                            <td style="witdh:10%">         
                                <a href="{{URL::route('admin.offers.edit', ['id' => $offer->id])}}"><i class="fa fa-pencil-square-o fa-2x"></i></a>
                                <a href="{{URL::route('admin.offers.destroy',['id' => $offer->id, '_token' => csrf_token()])}}" class="margin-left-5"><i class="fa fa-trash-o delete fa-2x"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>                
            @else
            <span class="text-warning"><h5>Нет предложений.</h5></span>
            @endif
			</div>
        </div>       
    </div>
</div>	
@stop 
@section('footer_scripts')
    {{ HTML::script('//code.jquery.com/ui/1.11.2/jquery-ui.js') }}
    <script>$("#sortable").sortable();</script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#sortable" ).sortable({
                cancel: ".ui-state-disabled",
                update: function(event, ui) { 
                    $.ajax({
                        data:{arr:$(this).sortable('toArray')} ,
                        type: 'POST',
                        url: '/admin/offers/order-update'
                    })
                }
            });
        });
    </script>
@stop