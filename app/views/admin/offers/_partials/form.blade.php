<div class="form-group">
	{{ Form::label('show','Отображать?') }}
	{{ Form::select('show',array('1' => 'Да','0' => 'Нет'),null,['class' => 'form-control']) }}	
</div>
<span class="text-danger">{{$errors->first('show')}}</span>
<div class="form-group">
	{{ Form::label('name','Заголовок') }}
	{{ Form::text('name',null,['class' => 'form-control']) }}	
</div>
<span class="text-danger">{{$errors->first('name')}}</span>
<div class="form-group">
	{{ Form::label('description','Описание') }}
	{{ Form::textarea('description',null,['class' => 'ckeditor']) }}		
</div>
<span class="text-danger">{{$errors->first('description')}}</span>
<div class="form-group">
	{{ Form::file('img')}}
</div>
{{Form::submit('Сохранить', array("class"=>"btn btn-info pull-right "))}}
