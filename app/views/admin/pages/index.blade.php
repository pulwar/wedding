@extends('laravel-authentication-acl::admin.layouts.base-2cols')

@section('title')
Admin area: dashboard
@stop

@section('content')	
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title bariol-thin"><i class="fa fa-pencil"> Страницы</i></h3>
            </div>            
            <div class="panel-body">
			<div class="row">                
            </div>
            @if(  count($pages) > 0 )
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Название</th>  
                        <th></th>
                    </tr>
                    </thead>
                    <tbody id="sortable">
                        @foreach($pages as $page)
                        <tr id="{{$page->id}}">
                            <td style="width:90%">{{$page->name}}</td> 
                            <td style="witdh:10%">         
                                <a href="{{URL::route('admin.pages.edit', ['id' => $page->id])}}"><i class="fa fa-pencil-square-o fa-2x"></i></a>                                
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>                
            @else
            <span class="text-warning"><h5>Нет страниц.</h5></span>
            @endif
			</div>
        </div>       
    </div>
</div>	
@stop 
