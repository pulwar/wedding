<div class="form-group">
	{{ Form::label('head','Title') }}
	{{ Form::text('head',null,['class' => 'form-control']) }}	
</div>
<span class="text-danger">{{$errors->first('head')}}</span>
<div class="form-group">
	{{ Form::label('mKeywords','Keywords') }}
	{{ Form::textarea('mKeywords',null,['class' => 'form-control']) }}	
</div>
<span class="text-danger">{{$errors->first('mKeywords')}}</span>
<div class="form-group">
	{{ Form::label('mDescription','Description') }}
	{{ Form::textarea('mDescription',null,['class' => 'form-control']) }}	
</div>
<span class="text-danger">{{$errors->first('mDescription')}}</span>

{{Form::submit('Сохранить', array("class"=>"btn btn-info pull-right "))}}
