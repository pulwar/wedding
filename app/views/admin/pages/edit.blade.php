@extends('laravel-authentication-acl::admin.layouts.base-2cols')

@section('title')
Admin area: dashboard
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title bariol-thin"><i class="fa fa-pencil"> {{$page->name}}</i></h3>
            </div>            
            <div class="panel-body">            	
           		{{ Form::model($page, array('route' => array('admin.pages.update',$page->id),'method' => 'post'))}}
					@include('admin.pages._partials.form')
				{{ Form::close()}}	
            </div>
        </div>       
    </div>
</div>	
@stop 