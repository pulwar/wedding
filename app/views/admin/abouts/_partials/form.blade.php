<div class="form-group">
	{{ Form::label('head_one','Заголовок-1') }}
	{{ Form::text('head_one',null,['class' => 'form-control']) }}	
</div>
<span class="text-danger">{{$errors->first('head_one')}}</span>
<div class="form-group">
	{{ Form::label('str_one','Текст-1') }}
	{{ Form::text('str_one',null,['class' => 'form-control']) }}	
</div>
<span class="text-danger">{{$errors->first('str_one')}}</span>
<div class="form-group">
	{{ Form::label('head_two','Заголовок-2') }}
	{{ Form::text('head_two',null,['class' => 'form-control']) }}	
</div>
<span class="text-danger">{{$errors->first('head_two')}}</span>
<div class="form-group">
	{{ Form::label('str_two','Текст-2') }}
	{{ Form::text('str_two',null,['class' => 'form-control']) }}	
</div>
<span class="text-danger">{{$errors->first('str_two')}}</span>
<div class="form-group">
	{{ Form::label('head_tree','Заголовок-3') }}
	{{ Form::text('head_tree',null,['class' => 'form-control']) }}	
</div>
<span class="text-danger">{{$errors->first('head_tree')}}</span>
<div class="form-group">
	{{ Form::label('str_tree','Текст-3') }}
	{{ Form::text('str_tree',null,['class' => 'form-control']) }}	
</div>
<span class="text-danger">{{$errors->first('str_tree')}}</span>
<div class="form-group">
	{{ Form::label('description','Описание') }}
	{{ Form::textarea('description',null,['class' => 'ckeditor']) }}		
</div>
<span class="text-danger">{{$errors->first('description')}}</span>
<div class="form-group">
	{{ Form::label('contacts','Контакты') }}
	{{ Form::textarea('contacts',null,['class' => 'ckeditor']) }}		
</div>
<span class="text-danger">{{$errors->first('contacts')}}</span>
<div class="form-group">
	{{ Form::file('img')}}
</div>
{{Form::submit('Сохранить', array("class"=>"btn btn-info pull-right "))}}
