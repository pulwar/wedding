@extends('laravel-authentication-acl::admin.layouts.base-2cols')

@section('title')
Admin area: dashboard
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title bariol-thin"><i class="fa fa-pencil"> {{$polycategory->id}}</i></h3>
            </div>            
            <div class="panel-body">            	
           		{{ Form::model($polycategory, array('route' => array('admin.polycategories.update',$polycategory->id),'method' => 'post','files' => true))}}
					@include('admin.polycategories._partials.form')
				{{ Form::close()}}	
            </div>
        </div>       
    </div>
</div>	
@stop 