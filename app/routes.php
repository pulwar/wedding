<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::resource('/','BaseController');
Route::get('/contacts',array('as' => 'contacts', 'uses' => 'BaseController@contacts'));
Route::post('/add-comment',array('as' => 'add.comment', 'uses' => 'BaseController@addComment'));
Route::get('/design',array('as' => 'design', 'uses' => 'BaseController@design'));
Route::get('/polygraphy',array('as' => 'polygraphy', 'uses' => 'BaseController@poly'));
Route::get('/offers',array('as' => 'offers', 'uses' => 'BaseController@offers'));
Route::get('/about',array('as' => 'about', 'uses' => 'BaseController@about'));

Route::group(array('prefix' => 'admin'/*, 'before' => "logged:/admin/login",*/
					), function(){
	Route::resource('pages', 'AdminPagesController',array('except' => array('update')));
	Route::post('pages/update/{sliders}',array('as' => 'admin.pages.update', 'uses' => 'AdminPagesController@update'));
	
	Route::resource('sliders', 'AdminSlidersController',array('except' => array('update','destroy')));	
	Route::post('sliders/update/{sliders}',array('as' => 'admin.sliders.update', 'uses' => 'AdminSlidersController@update'));
	Route::get('sliders/destroy/{slider}',array('as' => 'admin.sliders.destroy', 'uses' => 'AdminSlidersController@destroy'));	
	
	Route::resource('types', 'AdminTypesController');
	
	Route::resource('comments', 'AdminCommentsController',array('except' => array('update','destroy')));
	Route::post('comments/update/{comments}',array('as' => 'admin.comments.update', 'uses' => 'AdminCommentsController@update'));
	Route::get('comments/destroy/{comment}',array('as' => 'admin.comments.destroy', 'uses' => 'AdminCommentsController@destroy'));	

	Route::resource('designs', 'AdminDesignsController',array('except' => array('update','destroy')));
	Route::post('designs/update/{designs}',array('as' => 'admin.designs.update', 'uses' => 'AdminDesignsController@update'));
	Route::get('designs/destroy/{design}',array('as' => 'admin.designs.destroy', 'uses' => 'AdminDesignsController@destroy'));
	Route::post('designs/order-update',array('as' => 'admin.designs.order', 'uses' => 'AdminDesignsController@changeOrderOfDesign'));

	Route::resource('designimgs', 'AdminDesignImgsController',array('except' => array('update','destroy')));	
	Route::post('designimgs/update/{designs}',array('as' => 'admin.designimgs.update', 'uses' => 'AdminDesignImgsController@update'));
	Route::get('designimgs/destroy/{design}',array('as' => 'admin.designimgs.destroy', 'uses' => 'AdminDesignImgsController@destroy'));
	
	Route::resource('polycategories', 'AdminPolyCategoriesController',array('except' => array('update','destroy')));
	Route::post('polycategories/update/{id}',array('as' => 'admin.polycategories.update', 'uses' => 'AdminPolyCategoriesController@update'));
	Route::get('polycategories/destroy/{id}',array('as' => 'admin.polycategories.destroy', 'uses' => 'AdminPolyCategoriesController@destroy'));		
	Route::post('polycategories/order-update',array('as' => 'admin.polycategories.order', 'uses' => 'AdminPolyCategoriesController@changeOrderOfDesign'));	

	Route::resource('polygrafs', 'AdminPolygrafsController',array('except' => array('update','destroy','index')));	
	Route::post('polygrafs/update/{id}',array('as' => 'admin.polygrafs.update', 'uses' => 'AdminPolygrafsController@update'));
	Route::get('polygrafs/destroy/{id}',array('as' => 'admin.polygrafs.destroy', 'uses' => 'AdminPolygrafsController@destroy'));	

	Route::resource('offers', 'AdminOffersController',array('except' => array('update','destroy')));
	Route::post('offers/update/{id}',array('as' => 'admin.offers.update', 'uses' => 'AdminOffersController@update'));
	Route::get('offers/destroy/{id}',array('as' => 'admin.offers.destroy', 'uses' => 'AdminOffersController@destroy'));		
	
	Route::post('abouts/update/{id}',array('as' => 'admin.abouts.update', 'uses' => 'AdminAboutsController@update'));
	Route::get('abouts',array('as' => 'admin.abouts.edit', 'uses' => 'AdminAboutsController@edit'));	

	Route::resource('teams', 'AdminTeamsController',array('except' => array('update','destroy')));	
	Route::post('teams/update/{id}',array('as' => 'admin.teams.update', 'uses' => 'AdminTeamsController@update'));
	Route::get('teams/destroy/{id}',array('as' => 'admin.teams.destroy', 'uses' => 'AdminTeamsController@destroy'));	
	Route::post('teams/order-update',array('as' => 'admin.teams.order', 'uses' => 'AdminTeamsController@orderUpdate'));
});